package com.todoList.todoList;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TodoListRepository extends PagingAndSortingRepository<TodoList, Integer> {
}

