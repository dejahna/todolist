package com.todoList.todoList;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class TodoListIntrouvableException extends RuntimeException {

    public TodoListIntrouvableException(String s) {
        super(s);
    }
}