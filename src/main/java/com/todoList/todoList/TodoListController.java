package com.todoList.todoList;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;

@RestController
@RequestMapping(path = "todoLists")
public class TodoListController {

    @Autowired
    private TodoListRepository todoListRepository;

    @ApiOperation(value = "Récupère la liste des todoList")
    @GetMapping
    public Iterable<TodoList> getAllTodoList(Pageable pageable) {
        return this.todoListRepository.findAll(pageable);
    }

    @PostMapping
    public TodoList addTodoList(@Valid @RequestBody TodoList tl) {
        TodoList todoListAdded = todoListRepository.save(tl);
        if (todoListAdded == null) {
            System.err.print("Ko");
            return null;
        }
        //URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(todoListAdded.getId()).toUri();
        System.err.print(todoListAdded.toString());
        return todoListAdded;
    }

    @DeleteMapping("{id}")
    public ResponseEntity<Void> deleteTodoList(@PathVariable int id) {
        if (this.todoListRepository.findById(id).isPresent() == false) {
            return ResponseEntity.noContent().build();
        }
        this.todoListRepository.deleteById(id);
        return ResponseEntity.accepted().build();
    }

    @GetMapping("{id}")
    public TodoList getTodoList(@PathVariable int id) {
        Optional<TodoList> todoList = this.todoListRepository.findById(id);
        if (todoList.isPresent() == true) {
            return todoList.get();
        }
        throw new TodoListIntrouvableException("TodoList " + id + " introuvable");
    }

    @GetMapping("{id}/updateState")
    public TodoList updateState(@PathVariable int id) {
        Optional<TodoList> todoList = this.todoListRepository.findById(id);
        if (todoList.isPresent() == false) {
            throw new TodoListIntrouvableException("TodoList " + id + " introuvable");
        }
        todoList.get().setState(Status.DOING);
        this.todoListRepository.save(todoList.get());
        return todoList.get();
    }
}
