package com.todoList.todoList;

public enum Status {
    DOING,
    TODO,
    WIP
}
