import { Component, OnInit } from '@angular/core';
import { TodoModel } from '../../model/todoModel';
import { ActivatedRoute, Router } from '@angular/router';
import { StateEnum } from '../../enum/stateEnum';
import { Store } from '@ngrx/store';
import { DefinitionsAction } from '../../ngRx/actions/definitions.action';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'td-display-todo',
  templateUrl: './display-todo.component.html',
  styleUrls: ['./display-todo.component.scss']
})
export class DisplayTodoComponent implements OnInit {

  public localData: TodoModel;

  constructor(private route: ActivatedRoute, public store: Store<any>, private router: Router, private http: HttpClient) {
    this.route.params.subscribe((params) => {
      // this.store.select('appState').subscribe((res) => {
      //   this.localData = res[params.id];
      // });
      this.http.get<TodoModel>('/todoLists/' + params.id).subscribe((r) => {
        console.log(r);
        this.localData = r;
      });
    });
  }

  ngOnInit() {
  }

  done() {
    // this.store.dispatch({
    //   type: DefinitionsAction.TODO_DONE,
    //   payload: this.localData.id
    // });
    this.http.get('/todoLists/' + this.localData.id + '/updateState').subscribe(() => {
      this.router.navigate(['/listTodo']);
    });
  }

  cancel() {
    this.router.navigate(['/listTodo']);
  }
}
